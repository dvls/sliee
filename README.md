# Recursos de Software Libre para la Ingeniería Eléctrica y Electrónica

Sitio web realizado con Nikola y org-mode.

Disponible en <https://dvls.gitlab.io/sliee/>

Para probar localmente con docker:

```sh
docker run --rm -it -v $PWD:/site -w /site -u $(id -u):$(id -g) \
       -p 8888:8000 abellmann/nikola-orgmode nikola [command]
```

<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  <img alt="Licencia Creative Commons" style="border-width:0"
       src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>
<br />Esta obra está bajo una
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">
  Licencia Creative Commons Atribución-CompartirIgual 4.0 Internacional</a>.
