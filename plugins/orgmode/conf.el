(defun org-remove-headlines (backend)
  "Remove headlines with :no_title: tag."
  (org-map-entries (lambda () (let ((beg (point)))
                                (outline-next-visible-heading 1)
                                (backward-char)
                                (delete-region beg (point))))
                   "no_export" tree)
  (org-map-entries (lambda () (delete-region (point-at-bol) (point-at-eol)))
                   "no_title"))

(add-hook 'org-export-before-processing-hook #'org-remove-headlines)

;; override headline export for bootstrap panels

(defun org-html-headline (headline contents info)
  "Transcode a HEADLINE element from Org to HTML.
CONTENTS holds the contents of the headline.  INFO is a plist
holding contextual information."
  (unless (org-element-property :footnote-section-p headline)
    (let* ((contents (or contents ""))
	   (numberedp (org-export-numbered-headline-p headline info))
	   (level (org-export-get-relative-level headline info))
	   (text (org-export-data (org-element-property :title headline) info))
	   (todo (and (plist-get info :with-todo-keywords)
		      (let ((todo (org-element-property :todo-keyword headline)))
			(and todo (org-export-data todo info)))))
	   (todo-type (and todo (org-element-property :todo-type headline)))
	   (tags (and (plist-get info :with-tags)
		      (org-export-get-tags headline info)))
	   (priority (and (plist-get info :with-priority)
			  (org-element-property :priority headline)))
	   (section-number (mapconcat #'number-to-string
				      (org-export-get-headline-number
				       headline info) "-"))
	   (ids (delq 'nil
		      (list (org-element-property :CUSTOM_ID headline)
			    (concat "sec-" section-number)
			    (org-element-property :ID headline))))
	   (preferred-id (car ids))
	   (extra-ids (mapconcat
		       (lambda (id)
			 (org-html--anchor
			  (org-export-solidify-link-text
			   (if (org-uuidgen-p id) (concat "ID-" id) id))))
		       (cdr ids) ""))
	   ;; Create the headline text.
	   (full-text (org-html-format-headline--wrap headline info)))
      (if (org-export-low-level-p headline info)
	  ;; This is a deep sub-tree: export it as a list item.
	  (let* ((type (if numberedp 'ordered 'unordered))
		 (itemized-body
		  (org-html-format-list-item
		   contents type nil info nil
		   (concat (org-html--anchor preferred-id) extra-ids
			   full-text))))
	    (concat
	     (and (org-export-first-sibling-p headline info)
		  (org-html-begin-plain-list type))
	     itemized-body
	     (and (org-export-last-sibling-p headline info)
		  (org-html-end-plain-list type))))
	;; Standard headline.  Export it as a section.
	(let ((extra-class (org-element-property :HTML_CONTAINER_CLASS headline))
	      (level1 (+ level (1- org-html-toplevel-hlevel)))
	      (first-content (car (org-element-contents headline)))
	      (panel-type (org-element-property :PANEL_TYPE headline))
	      (panel (org-element-property :PANEL headline)))
	  (if (equal panel "panel-group")
	      (format "<%s id=\"%s\" class=\"%s\">%s%s</%s>\n"
		      (org-html--container headline info)
		      (format "outline-container-%s"
			      (or (org-element-property :CUSTOM_ID headline)
				  (concat "sec-" section-number)))
		      (concat (format "outline-%d" level1) (and extra-class " ")
			      extra-class)
		      (format "\n<h%d id=\"%s\">%s%s</h%d>\n"
			      level1 preferred-id extra-ids full-text level1)
		      (format "<div class=\"panel-group\" id=\"%s\" role=\"tablist\" aria-multiselectable=\"true\">%s</div><!-- panel-group -->\n"
			      (format "outline-container-%s"
				      (or (org-element-property :PANEL_ID headline)
					  (concat "panel-group" section-number)))
			      (if (not (eq (org-element-type first-content) 'section))
				  ()
				;;(concat (org-html-section first-content "" info)
				;;	  contents)
				contents))
		      (org-html--container headline info))
	    (if (equal panel "panel")
		(format "%s"
			(format "<div class=\"%s\">\n%s%s</div><!-- panel -->\n"
				(concat panel " "panel-type)
				;;heading
				(format "<div class=\"panel-heading\" role=\"tab\" id=\"%s\">\n%s</div><!-- panel heading -->\n"
					;; id
					(concat "panel-head-" section-number)
					;; <h>
					(format "<h%d class=\"panel-title\" id=\"%s\">\n%s</h%d>\n"
						level1
						preferred-id
						(format "<a class=\"collapsed\" role=\"button\" data-toggle=\"collapse\" href=\"%s\" aria-expanded=\"false\" aria-controls=\"%s\">%s%s</a>\n"
							(concat "#collapse-" section-number)
							(concat "collapse-" section-number)
							extra-ids
							full-text)
						level1))
				;;panel collapse and body
				(format "<div id=\"%s\" class=\"panel-collapse collapse %s\" role=\"tabpanel\" aria-labelledby=\"%s\">\n%s</div><!-- panel-colapse -->\n"
					(concat "collapse-" section-number)
					(if (org-element-property :PANEL_ON headline) (concat "in") (concat "") )
					(concat "panel-head-" section-number)
					(format "<div class=\"panel-body\">\n%s</div><!-- panel-body -->\n"
						;; content
						(if (not (eq (org-element-type first-content) 'section))
						    (concat (org-html-section first-content "" info)
							    contents)
						  contents)))))
	      (format "<%s id=\"%s\" class=\"%s\">%s%s</%s>\n"
		      (org-html--container headline info)
		      (format "outline-container-%s"
			      (or (org-element-property :CUSTOM_ID headline)
				  (concat "sec-" section-number)))
		      (concat (format "outline-%d" level1) (and extra-class " ")
			      extra-class)
		      (format "\n<h%d id=\"%s\">%s%s</h%d>\n"
			      level1 preferred-id extra-ids full-text level1)
		      ;; When there is no section, pretend there is an
		      ;; empty one to get the correct <div class="outline-
		      ;; ...> which is needed by `org-info.js'.
		      (if (not (eq (org-element-type first-content) 'section))
			  (concat (org-html-section first-content "" info)
				  contents)
			contents)
		      (org-html--container headline info)))))))))
  
