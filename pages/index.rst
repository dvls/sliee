.. title: Recursos de Software Libre para la Ingeniería Eléctrica y Electrónica
.. slug: index
.. date: 2020-09-08 19:02:23 UTC-03:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Este sitio recopila algunas herramientas disponibles que pueden ser utilizadas
por profesionales de la ingeniería eléctrica y electrónica para ejercer su
profesión.

Para ver los recursos, por favor sigue los enlaces en la barra de navegación o
selecciona una sección a continuación:

* `Ciencias Básicas <ccbb>`_
* `Ofimática <ofimatica>`_
* `Ingeniería Eléctrica y Electrónica <iee>`_
* `Más recursos <otros-recursos>`_

.. contents::
.. section-numbering::
